# UO Maps CIS-407 Project #
Super lightweight and simple dockerized nodeJS server.

### Dependencies:###
-NodeJS

-MongoDB

-Docker

### Running it ###
After you clone this baby, just run these docker commands:

`docker-compose build`  In your home directory

`docker-compose up`     Right after

That should hopefully be it! I'm relatively new to docker, so let me know if there are any issues at: bcarl@uoregon.edu