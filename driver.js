// Written by:
// Bryan Carl
// Create globals so leaflet can load
global.win = {};
global.window = {};
global.Win = {};

global.navigator = {
  userAgent: 'nodejs'
};

const express = require('express');
const mongodb = require('mongodb');
const path    = require('path');

// Network
const PORT = 5000;
const HOST = '0.0.0.0';

// Express instance
const app = express();

app.get('/', (req, res) => {
  app.use(express.static('html'));
  res.sendFile(path.join(__dirname + "/html/template.html"));
});

app.get('/search', (req, res) => {
  app.use(express.static('html'));
  res.sendFile(path.join(__dirname+'/html/template.html'));
});

app.get('/login', (req, res) => {
  app.use(express.static('html'));
  res.sendFile(path.join(__dirname+'/html/template.html'));
});

app.post('/input', (req, res) => {
})

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
