// Written by:
// Bryan Carl

module.exports = {
    DB: 'mongodb://mongo:27017/newdock'
}

// Tried Messing around with mongoose stuff for login auth
//var UserSchema = new mongoose.Schema({
//   email: {
//     type: String,
//     unique: true,
//     required: true,
//     trim: true
//   },
//   username: {
//     type: String,
//     unique: true,
//     required: true,
//     trim: true
//   },
//   password: {
//     type: String,
//     required: true,
//   }
// });
// var User = mongoose.model('User', UserSchema);
// module.exports = User;
